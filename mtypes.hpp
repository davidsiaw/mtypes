#ifndef MTYPES_HPP
#define MTYPES_HPP

#include <iostream>
#include <string>
#include <memory>
#include <sstream>
#include <vector>
#include <iomanip>

#undef minor

class MatrixDimensionMustBeAtLeast1 {};
class MatrixDimensionsDoNotMatch {};
class InitializerSizeNotSizeOfMatrix {};
class OperationOnlyAllowedOnSquareMatrix {};
class MatrixNotBigEnough {};
class MatrixHasNoInverse {};

/* Matrix class
 * @MatrixElementType the type that this matrix carries
 */
template<typename MatrixElementType = double>
class Matrix
{
	unsigned int rows;
	unsigned int columns;
	std::shared_ptr<MatrixElementType> array;
	unsigned int array_size;

	void zero()
	{
		for (int i=0;i<rows;i++)
		{
			for (int j=0;j<columns;j++)
			{
				set(i,j,0);
			}
		}
	}

public:

	/* Returns an identity matrix of a given size
	 * @size the number of rows and columns for the matrix
	 *
	 * Creates a square matrix that is also an identity matrix
	 *
	 * @return a square identity matrix
	 */
	static Matrix identity(unsigned int size)
	{
		Matrix m(size, size);
		for (int i=0;i<size;i++)
		{
			m(i,i) = 1;
		}
		return m;
	}

	Matrix(unsigned int m, unsigned int n) :
	rows(m), columns(n), array_size(m*n)
	{
		if (rows < 1 || columns < 1)
		{
			throw MatrixDimensionMustBeAtLeast1();
		}
		array = std::shared_ptr<MatrixElementType>( new MatrixElementType[array_size], std::default_delete<MatrixElementType[]>() ); 
		zero();
	}

	Matrix(unsigned int m, unsigned int n, std::vector<MatrixElementType> initializer) : Matrix(m,n)
	{
		if (initializer.size() != rows*columns)
		{
			throw InitializerSizeNotSizeOfMatrix();
		}
		for (int i=0;i<rows;i++)
		{
			for (int j=0;j<columns;j++)
			{
				set(i,j, initializer[i * columns + j]);
			}
		}
	}

	Matrix(const Matrix& other) : rows(other.rows), columns(other.columns), array_size(rows*columns)
	{
		array = std::shared_ptr<MatrixElementType>( new MatrixElementType[array_size], std::default_delete<MatrixElementType[]>() );

		for (int i=0;i<rows;i++)
		{
			for (int j=0;j<columns;j++)
			{
				set(i,j, other(i,j));
			}
		}
	}

	Matrix& operator=(const Matrix& other)
	{
		rows = other.rows;
		columns = other.columns;
		array = std::shared_ptr<MatrixElementType>( new MatrixElementType[array_size], std::default_delete<MatrixElementType[]>() );

		for (int i=0;i<rows;i++)
		{
			for (int j=0;j<columns;j++)
			{
				set(i,j, other(i,j));
			}
		}

		return *this;
	}

	~Matrix()
	{ }

	bool is_square() const
	{
		return rows == columns;	
	}

	Matrix submatrix(unsigned int row, unsigned int column) const
	{
		if (rows < 2 || columns < 2)
		{
			throw MatrixNotBigEnough();
		}
		row = row % rows;
		column = column % columns;

		Matrix new_matrix(rows - 1, columns - 1);

		for (int i=0;i<rows - 1;i++)
		{
			for (int j=0;j<columns - 1;j++)
			{
				auto ii = i;
				auto jj = j;
				if (i >= row)
				{
					ii = i + 1;
				}
				if (j >= column)
				{
					jj = j + 1;
				}
				new_matrix(i,j) = get(ii,jj);
			}
		}

		return new_matrix;
	}

	Matrix transpose() const
	{
		Matrix new_matrix(columns, rows);

		for (int i=0;i<rows;i++)
		{
			for (int j=0;j<columns;j++)
			{
				new_matrix.set(j,i, get(i,j) );
			}
		}

		return new_matrix;
	}

	std::string to_string() const
	{
		std::vector<std::string> string_list;
		size_t max_size = 0;
		for (int i=0;i<rows;i++)
		{
			for (int j=0;j<columns;j++)
			{
				std::stringstream str;
				str << get(i,j);
				auto size = str.str().size();
				if (size > max_size)
				{
					max_size = size;
				}
				string_list.push_back(str.str());
			}
		}

		std::stringstream ss;
		for (int i=0;i<rows;i++)
		{
			if (rows != 1)
			{
				if (i==0)
				{
					ss << "⎡";
				}
				else if (i==rows-1)
				{
					ss << "⎣";
				}
				else 
				{
					ss << "⎢";
				}
			}
			else
			{
				ss << "[";
			}

			for (int j=0;j<columns;j++)
			{
				if (j) { ss << ", "; }
				auto curr_string = string_list[i * columns + j];
				ss << std::setw(max_size) << curr_string;
			}

			if (rows != 1)
			{
				if (i==0)
				{
					ss << "⎤" << std::endl;
				}
				else if (i==rows-1)
				{
					ss << "⎦" << std::endl;
				}
				else 
				{
					ss << "⎥" << std::endl;
				}
			}
			else
			{
				ss << "]" << std::endl;
			}
		}
		return ss.str();
	}

	MatrixElementType get(unsigned int i, unsigned int j) const
	{
		return array.get()[(i + rows * j) % array_size];
	}

	void set(unsigned int i, unsigned int j, MatrixElementType value)
	{
		array.get()[(i + rows * j) % array_size] = value;
	}

	unsigned int get_rows() const
	{
		return rows;
	}

	unsigned int get_columns() const
	{
		return columns;
	}

	MatrixElementType minor(unsigned int row, unsigned int column) const
	{
		return submatrix(row,column).determinant();
	}

	MatrixElementType cofactor(unsigned int row, unsigned int column) const
	{
		MatrixElementType sign = -1;
		if ((row + column) % 2 == 0) { sign = 1; }
		return sign * minor(row, column);
	}

	Matrix comatrix() const
	{
		Matrix new_matrix(rows, columns);

		for (int i=0;i<rows;i++)
		for (int j=0;j<columns;j++)
		{
			new_matrix(i,j) = cofactor(i,j);
		}

		return new_matrix;
	}

	Matrix adjoint() const
	{
		return comatrix().transpose();
	}

	Matrix inverse() const
	{
		if (determinant() == 0)
		{
			throw MatrixHasNoInverse();
		}
		return 1/determinant() * comatrix().transpose();
	}

	MatrixElementType trace() const
	{
		if (!is_square())
		{
			throw OperationOnlyAllowedOnSquareMatrix();
		}
		MatrixElementType total = 0;
		for (int i=0;i<rows;i++)
		{
			total += get(i,i);
		}
		return total;
	}

	MatrixElementType determinant() const
	{
		if (!is_square())
		{
			throw OperationOnlyAllowedOnSquareMatrix();
		}
		if (rows == 1)
		{
			// Degenerate case
			return get(0,0);
		}
		if (rows == 2)
		{
			// 2x2 case
			return get(0,0) * get(1,1) - get(0,1) * get(1,0);
		}

		// Other cases
		MatrixElementType total = 0;
		for (int i=0;i<rows;i++)
		{
			total += get(0,i) * cofactor(0,i);
		}

		return total;
	}

	Matrix add(const Matrix& another) const
	{
		if (columns != another.columns || rows != another.rows)
		{
			throw MatrixDimensionsDoNotMatch();
		}
		Matrix new_matrix(rows, another.columns);

		for (int i=0;i<rows;i++)
		{
			for (int j=0;j<columns;j++)
			{
				new_matrix(i,j) = get(i,j) + another(i,j);
			}
		}

		return new_matrix;
	}

	Matrix multiply(const Matrix& another) const
	{
		if (columns != another.rows)
		{
			throw MatrixDimensionsDoNotMatch();
		}
		Matrix new_matrix(rows, another.columns);

		for (int i=0;i<rows;i++)
		{
			for (int j=0;j<columns;j++)
			{
				MatrixElementType total = 0;
				for (int col=0;col<columns;col++)
				{
					total += get(i,col) * another(col,j);
				}
				new_matrix(i,j) = total;
			}
		}

		return new_matrix;
	}

	Matrix multiply(MatrixElementType scalar) const
	{
		Matrix new_matrix(rows, columns);

		for (int i=0;i<rows;i++)
		{
			for (int j=0;j<columns;j++)
			{
				new_matrix.set(i,j, get(i,j) * scalar);
			}
		}
		return new_matrix;
	}

	MatrixElementType& operator() (unsigned i, unsigned j)
	{
		return array.get()[(i + rows * j) % array_size];
	}

	MatrixElementType operator() (unsigned i, unsigned j) const
	{
		return array.get()[(i + rows * j) % array_size];
	}

	Matrix operator- () const
	{
		return this->multiply(-1);
	}

};

template<typename MatrixElementType>
Matrix<MatrixElementType> operator+ (const Matrix<MatrixElementType>& x, const Matrix<MatrixElementType>& y)
{
	return x.add(y);
}

template<typename MatrixElementType>
Matrix<MatrixElementType> operator- (const Matrix<MatrixElementType>& x, const Matrix<MatrixElementType>& y)
{
	return x.add(-y);
}

// Matrix multiply
template<typename MatrixElementType>
Matrix<MatrixElementType> operator* (const Matrix<MatrixElementType>& x, const Matrix<MatrixElementType>& y)
{
	return x.multiply(y);
}

template<typename MatrixElementType>
Matrix<MatrixElementType> operator* (const Matrix<MatrixElementType>& x, MatrixElementType value)
{
	return x.multiply(value);
}

template<typename MatrixElementType>
Matrix<MatrixElementType> operator* (MatrixElementType value, const Matrix<MatrixElementType>& x)
{
	return x.multiply(value);
}

// Matrix division
template<typename MatrixElementType>
Matrix<MatrixElementType> operator/ (const Matrix<MatrixElementType>& x, const Matrix<MatrixElementType>& y)
{
	return y.inverse().multiply(x);
}

template<typename MatrixElementType>
Matrix<MatrixElementType> operator/ (const Matrix<MatrixElementType>& x, MatrixElementType value)
{
	return x.multiply(1/value);
}

template<typename MatrixElementType>
Matrix<MatrixElementType> operator/ (MatrixElementType value, const Matrix<MatrixElementType>& x)
{
	return x.inverse().multiply(value);
}

template<typename MatrixElementType>
std::ostream &operator<<(std::ostream &os, Matrix<MatrixElementType> const &m)
{ 
    return os << m.to_string();
}

template<typename MatrixElementType = double>
class Vector : public Matrix<MatrixElementType>
{
public:
	Vector(unsigned int n) : Matrix<MatrixElementType>(n, 1)
	{ }

	Vector(std::vector<MatrixElementType> initializer) : Matrix<MatrixElementType>(initializer.size(), 1)
	{ 
		for (int i=0;i<initializer.size();i++)
		{
			this->set(i,0,initializer[i]);
		}
	}
};

#endif // MTYPES_HPP
