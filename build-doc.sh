cldoc generate -std=c++11 -- mtypes.hpp main.cpp --type xml --output doc
pushd doc
	bundle install
	bundle exec weaver build -r /mtypes/
popd
mv doc/build public
