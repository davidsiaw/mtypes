#include <iostream>
#include "mtypes.hpp"

int main()
{
	Matrix<> m(2,3);
	m(0,0) = 500;
	m(1,0) = 20;

	std::cout << m << std::endl;

	std::cout << m.transpose() << std::endl;

	Vector<> v(5);
	std::cout << v << std::endl;

	Matrix<> id = Matrix<>::identity(3);
	std::cout << id << std::endl;

	std::cout << id * 6.0 << std::endl;

	std::cout << 6.0 * id << std::endl;

	std::cout << m * m.transpose() << std::endl;

	Matrix<> inited(3,3,
	{	 1,2,0,
		-1,1,1,
		 1,2,3	});

	std::cout << inited << std::endl;
	std::cout << inited.determinant() << std::endl;

	for (int i=0;i<3;i++)
	for (int j=0;j<3;j++)
	{
		std::cout << inited.submatrix(i,j) << std::endl;
	}

	Vector<> vect({3,4,5});

	std::cout << inited * vect << std::endl;

	std::cout << vect.transpose() << std::endl;

	Matrix<> four(4,4,
	{	 1,2,3,4,
		 5,6,7,8,
		 2,6,4,8,
		 3,1,1,2	});

	std::cout << four << std::endl;
	std::cout << four.determinant() << std::endl;
	std::cout << four.comatrix() << std::endl;
	std::cout << four.adjoint() << std::endl;
	std::cout << four.inverse() << std::endl;
	std::cout << four.inverse() * four << std::endl;
	std::cout << four / four << std::endl;


	Matrix<> x(3,3,
	{	 1,2,3,
		 0,4,5,
		 1,0,6	});

	std::cout << x << std::endl;
	std::cout << x.determinant() << std::endl;
	std::cout << x.comatrix() << std::endl;
	std::cout << x.adjoint() << std::endl;
	std::cout << x.inverse() << std::endl;
	std::cout << x.inverse() * x << std::endl;
	std::cout << x / x << std::endl;


	Matrix<> two(2,2,
	{	 1,2,
		 0,4	});

	std::cout << two << std::endl;
	std::cout << two.determinant() << std::endl;
	std::cout << two.comatrix() << std::endl;
	std::cout << two.adjoint() << std::endl;
	std::cout << two.inverse() << std::endl;
	std::cout << two.inverse() * two << std::endl;

	return 0;
}
