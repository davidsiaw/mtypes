
class Hash
  def to_xml(options = {})
    require 'xmlsimple'
    options.delete(:builder)
    options.delete(:skip_instruct)
    options.delete(:dasherize)
    options.delete(:skip_types)

    options[:Indent] = ' '*options.delete(:indent) unless options[:indent].nil?
    options[:RootName] = options.delete(:root)

    XmlSimple.xml_out(self, options)
  end

  class << self
    def from_xml(xml, disallowed_types = nil)
      require 'xmlsimple'
      XmlSimple.xml_in(xml, KeepRoot: true, SuppressEmpty: true, KeyToSymbol: false, ForceArray: false)
    end
  end
end

def page(url, title, data, subdata, &block)

	sidenav_page "#{url}", "#{title}" do

		menu do
			nav "Home", :"th-large", "/"

			subdata.each do |key, value|
				options = {}
				if title.include? "#{key}"
					options["class"] = "active"
				end
				nav "#{key} class", :square, "/#{key.downcase}", options do

					nav "#{key}<>", :square, "/#{key.downcase}"

					value["classtemplate"]["method"].each do |method|

						item_icon = :"circle-o"
						if method["static"] == "yes"
							item_icon = :circle
						end

						nav method["name"], item_icon, "/#{key.downcase}.#{method["name"]}"
					end if value["classtemplate"]["method"]
					#value["classtemplate"]["method"].each do |method|
					#	nav "#{method["name"]}", :square, "/#{key.downcase}.#{method["name"]}" 
					#end
				end
			end

			nav "Exceptions", :"th-large", "/exception"
		end

		header do
			col 12 do
				h1 "#{title}"
			end
		end

		instance_eval(&block)
	end

end

